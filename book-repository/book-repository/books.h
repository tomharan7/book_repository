#ifndef BOOKS_H
#define BOOKS_H

#include <iostream>
using namespace std;

class Books
{
public:
	Books(int id_param, string author_param);

	int get_ID();

	void set_ID(int id_param);

	string get_Author();

	void set_Author(string author_param);

private:
	int ID;
	string Author;
}

#endif